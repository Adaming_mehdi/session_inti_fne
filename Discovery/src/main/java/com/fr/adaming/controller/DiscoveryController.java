package com.fr.adaming.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fr.adaming.dto.MicroServiceDto;

@RestController
@RequestMapping(path = "/discovery")
public class DiscoveryController {

	private static List<MicroServiceDto> allMicroServices = new ArrayList<>();
	
	//Register
	@PostMapping(path = "/register")
	public String register(@RequestBody MicroServiceDto dto) {
		allMicroServices.add(dto);
		
		return "register MicroService SUCCESS";
	}
	
	//GetMSAdresseByName
	@GetMapping(path = "/name/{name}")
	public MicroServiceDto getMsByName(@PathVariable String name) {
		MicroServiceDto result = null;
		//Parcour la liste des microServices inscrits
		for(MicroServiceDto dto : allMicroServices) {
			if(dto.getName().equals(name)) {
				result = dto;
				break;
			}
		}
		
		return result;
	}
	
	@GetMapping
	public List<MicroServiceDto> getAllRegistredMicroServices(){
		return allMicroServices;
	}
}
