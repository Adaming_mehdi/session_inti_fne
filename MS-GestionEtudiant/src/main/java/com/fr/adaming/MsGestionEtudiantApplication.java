package com.fr.adaming;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsGestionEtudiantApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsGestionEtudiantApplication.class, args);
	}

}
