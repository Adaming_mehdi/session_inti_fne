package com.fr.adaming.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.fr.adaming.dao.IEtudiantDao;
import com.fr.adaming.dto.EtudiantRegisterDto;
import com.fr.adaming.dto.MicroServiceDto;
import com.fr.adaming.entity.Etudiant;

@RestController
public class EtudiantController {
	
	RestTemplate rest = new RestTemplate();
	
	@Autowired	// Inversion Of Control == Dependency Injection
	private IEtudiantDao dao;

	
	@RequestMapping(method = RequestMethod.GET, path = "/hello")
	public String sayHello() {
		return "Hello World From MS-GestionEtudiant";
	}
	
	
	//Data Transfert Object 
	@RequestMapping(method = RequestMethod.POST, path = "/etudiant")
	public String inscription(@RequestBody EtudiantRegisterDto dto) {
		
		//Demander la date de début de la session dispo
		//L'@ du serveur de microservice MS-GestionFormation et le numéro de port
		
		//Demander l'@ip et le numéro de port du MS-GestionFormation à Discovery
		
		MicroServiceDto responseMs = rest.getForObject("http://localhost:9090/discovery/name/MS-GestionFormation", MicroServiceDto.class);
		
		System.err.println("DEBUG response MicroService-Formation : " + responseMs);
		
		//url de web service : /date
		//Body : rien
		//PathVariable : rien
		//Response Type : String
		//Status OK : 200
		//Status Error : 400 (BadRequest), 404 (NotFound)
		
		String adresse = responseMs.getAdresse() + "/formation/date/"+dto.getVille();
		System.out.println("DEBUG adress MS-GestionFormation getByDateAndVille :  " + adresse);
		
		String response = rest.getForObject(adresse, String.class);
		
		System.err.println("DEBUG response MS-GestionFormation ===> "+ response);
		
		//Finir l'inscription
		Etudiant entity = new Etudiant();
		entity.setAge(dto.getAge());
		entity.setNom(dto.getNom());
		entity.setPrenom(dto.getPrenom());
		entity.setEmail(dto.getEmail());
		entity.setFormationUrl(responseMs.getAdresse()+"/formation/id/"+response);
		
		//Enregistrer l'entity dans la BD
		System.err.println("DEBUG result entity ==> "+ entity);
		dao.save(entity);
		
		//retourner de message de confirmation
		return "SUCCESS";
	}
	
	@GetMapping
	public List<Etudiant> readAll(){
		return dao.findAll();		//SELECT * FROM etudiant
	}
}
