package com.fr.adaming.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @NoArgsConstructor @AllArgsConstructor @ToString
@Entity	//JPA
public class Etudiant {

	@Id	//Java Persistence API (javax.persistence)
	@GeneratedValue(strategy = GenerationType.IDENTITY)		//JPA
	private Long id;

	@Column(nullable = false)
	private String nom;

	@Column(length = 60, name = "first_name")
	private String prenom;

	@Column(unique = true)	//JPA
	private String email;

	private Integer age;
	
	private String ville;
	
	private String formationUrl;

}
