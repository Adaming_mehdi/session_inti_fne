package com.fr.adaming.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;

@Data @AllArgsConstructor
public class EtudiantRegisterDto {

	@NonNull
	private String nom;

	private String prenom;

	private String email;
	
	private String ville;

	@NonNull
	private Integer age;
	
	
}
