package com.fr.adaming.repository;

import java.time.LocalDate;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fr.adaming.entity.Formation;

public interface IFormationRepo extends JpaRepository<Formation, Long>{

	Formation findByVilleAndDateDebutFormationAfter(String ville, LocalDate now);
}
