package com.fr.adaming.entity;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Formation {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String label;

	private LocalDate dateDebutFormation;

	private String ville;

	public Formation(String label, LocalDate dateDebutFormation, String ville) {
		super();
		this.label = label;
		this.dateDebutFormation = dateDebutFormation;
		this.ville = ville;
	}

}
