package com.fr.adaming;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import com.fr.adaming.dto.MicroServiceDto;

@Configuration
public class RegisterDiscovery {

	
	public RegisterDiscovery(@Value("${spring.application.name}") String appName, @Value("${server.port}") int port, @Value("${discovery.register.url}") String discoveryRegisterUrl) {
		MicroServiceDto dto = new MicroServiceDto(appName, "http://localhost:"+port);
		
		RestTemplate rest = new RestTemplate();
		
		String response = rest.postForObject(discoveryRegisterUrl, dto, String.class);
		
		System.err.println("RegisterDiscovery reponse : " + response);
	}
}
