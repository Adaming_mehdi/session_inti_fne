package com.fr.adaming.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor @AllArgsConstructor
public class FormationUpdateDto {

	private Long id;

	private String label;

	private String dateDebutFormation;

	private String ville;
}
