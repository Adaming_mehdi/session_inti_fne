package com.fr.adaming.controller.generic;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping(path = "/formation")
public interface IGenericController<ENTITY, ID> {	// interface == contrat

	//Create
	@PostMapping
	public String create(@RequestBody ENTITY entity);
	
	//ReadAll
	@GetMapping
	public List<ENTITY> readAll();
	
	//ReadById
	@GetMapping(path = "/id/{id}")
	public ENTITY readById(@PathVariable ID id);
	
	//Update
	@PutMapping
	public String update(@RequestBody ENTITY entity);
	
	//DeleteById
	@DeleteMapping(path = "/id/{id}")
	public String deleteById(@PathVariable ID id);
}
