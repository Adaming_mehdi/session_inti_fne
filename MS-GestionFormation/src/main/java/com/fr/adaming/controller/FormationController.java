package com.fr.adaming.controller;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fr.adaming.controller.generic.IGenericController;
import com.fr.adaming.entity.Formation;
import com.fr.adaming.repository.IFormationRepo;

@RestController
public class FormationController implements IGenericController<Formation, Long>{
	
	@Autowired
	private IFormationRepo dao;

	@RequestMapping(method = RequestMethod.GET, path = "/hello")
	public String sayHello() {
		return "Hello World From MS-GestionFormation";
	}
	
	@GetMapping( path = "/date/{ville}")
	public Long getDateDebutDispo(@PathVariable String ville) {
		//TODO Chercher dans la BD la date de début de la session dispo dans cette ville
		Formation f = dao.findByVilleAndDateDebutFormationAfter(ville, LocalDate.now());
		
		return f.getId();
	}

	@Override	//redéfinition ||| Overload : surcharge
	public String create(Formation entity) {
		Formation response = dao.save(entity); //Insert into
		
		if(response != null) {
			return "SUCCESS";
		}
		return "FAIL";
	}

//	@Override
	public List<Formation> readAll() {
		return dao.findAll();
	}

//	@Override
	public Formation readById(Long id) {
		return dao.findById(id).get();
	}

//	@Override
	public String update(Formation entity) {
		if(entity != null && entity.getId() != null && dao.existsById(entity.getId())) {
//			Formation entity = new Formation(dto.getId(), dto.getLabel(), dto.getDateDebutFormation(), dto.getVille());
			dao.save(entity);	//Update formation set xx where id = ?
			return "SUCCESS";
		}
		return "FAIL";
	}

//	@Override
	public String deleteById(Long id) {
		try {
			dao.deleteById(id);
			return "SUCCESS";
		}catch (Exception e) {
			e.printStackTrace();
			return "FAIL";
		}
	}
}
